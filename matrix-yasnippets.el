;;; matrix-yasnippets.el --- Quickly enter matrix-like structures in multiple languages  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Thibaut Verron

;; Author: Thibaut Verron <thibaut.verron@gmail.com>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

;; Specification:
;;
;; - Call the function the number of rows and columns, and then fill the matrix
;;   ala yasnippet
;; - Give special arguments per language, eg coefficient ring in python,
;;   matrix style in latex...
;; - Set an entry as the default for next cells or for all cells or for this
;;   column or this row
;; - Move up and down in columns

(defvar maya-latex--mtx-type "")

(defun maya-latex-prompt-mtx-type ()
  (setq maya-latex--mtx-type
	(completing-read
	 "Matrix type? "
	 (list "matrix" "smallmatrix"
	       "pmatrix" "bmatrix" "Bmatrix"
	       "vmatrix" "Vmatrix"))))

(defun maya-latex-begin ()
  (concat "\\begin{" maya-latex--mtx-type "}"))

(defun maya-latex-end ()
  (concat "\\end{" maya-latex--mtx-type "}"))

(defvar maya-spec-default
  '(:begin "[" :end "]" :rbeg "[" :rend "]" :rsep "," :fsep ","))

(defvar maya-long-specs-per-mode
  '((latex-mode
     . (:pre maya-latex-prompt-mtx-type
	:begin maya-latex-begin :end maya-latex-end :rbeg "" :rend ""
	:rsep " \\\\\\\\ " :fsep " & "))))

(defvar maya-short-specs-per-mode
  '((latex-mode
     . (:begin "" :end "" :rbeg "" :rend ""
	:rsep " \\\\\\\\ " :fsep " & "))))

(defun maya-rowcol-index (row col ncols)
  (+ (* row ncols) col 1))

(defun maya-plist-get (spec elt)
  (let ((val (plist-get spec elt)))
    (cond
     ((eq val nil) "")
     ((stringp val) val)
     ((functionp val) (funcall val))
     (t (error "Maya: I do not know what to do with %s" val)))))


(defvar maya--row 0)
(defvar maya--col 0)
(defvar maya--nrows 0)
(defvar maya--ncols 0)

(defvar maya--debug nil)
(setq maya--debug t)

(defun maya--message-info ()
  (when maya--debug
    (message (format "Row %s/%s // Column %s/%s"
		     maya--row maya--nrows
		     maya--col maya--ncols))))

(defun maya--previous-row ()
  (interactive)
  (unless (= maya--row 1)
    (dotimes (i maya--ncols)
      (yas-prev-field))
    (setq maya--row (- maya--row 1)))
  (maya--message-info))

(defun maya--next-row ()
  (interactive)
  (unless (= maya--row maya--ncols)
    (dotimes (i maya--ncols)
      (yas-next-field))
    (setq maya--row (+ maya--row 1)))
  (maya--message-info))

(defun maya--previous-field ()
  (interactive)
  (unless (and (= maya--row 1) (= maya--col 1))
    (yas-prev-field)
    (if (= maya--col 1)
	(setq maya--col maya--ncols
	      maya--row (- maya--row 1))
      (setq maya--col (- maya--col 1))))
  (maya--message-info))

(defun maya--next-field ()
  (interactive)
  (unless (and (= maya--row maya--nrows)
	       (= maya--col maya--ncols))
    (yas-next-field)
    (if (= maya--col maya--ncols)
	(setq maya--col 1
	      maya--row (+ maya--row 1))
      (setq maya--col (+ maya--col 1))))
  (maya--message-info))

(setq maya-expand-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map yas-keymap)
    (define-key map (kbd "<return>") #'maya--next-row)
    (define-key map (kbd "S-<return>") #'maya--previous-row)
    (define-key map (kbd "<tab>") #'maya--next-field)
    (define-key map (kbd "S-<tab>") #'maya--previous-field)
    (define-key map (kbd "<backtab>") #'maya--previous-field)
    ;; (define-key map [remap 'yas/next-field] #'maya-next-field)
    ;; (define-key map [remap 'yas/previous-field] #'maya-previous-field)
    (define-key map (kbd "C-S-n") #'maya--next-row)
    (define-key map (kbd "C-S-p") #'maya--previous-row)
    (define-key map (kbd "C-S-f") #'maya--next-field)
    (define-key map (kbd "C-S-b") #'maya--previous-field)
    map))


(defun maya-insert-matrix ()
  (interactive)
  (let ((spec (alist-get major-mode maya-short-specs-per-mode maya-spec-default))
	(beginp (point)))
    (let ((pre-fun (plist-get spec :pre)))
      (and pre-fun (funcall pre-fun)))
    (let* ((nrows (read-number "Number of rows? " 2))
	   (ncols (read-number "Number of columns? " nrows))
	   (add_lf (yes-or-no-p "Newlines? ")))
      (insert (maya-plist-get spec :begin))
      ;; (when add_lf (insert "\n"))
      (dotimes (row nrows nil)
	(insert (maya-plist-get spec :rbeg))
	(dotimes (col ncols nil)
	  (insert "${"
		  (number-to-string
		   (maya-rowcol-index row col ncols))
		  ":a" 
		  (number-to-string (+ row 1))
		  (number-to-string (+ col 1))
		  "}")
	  (unless (eq col (- ncols 1))
	    (insert (maya-plist-get spec :fsep))))
	(insert (plist-get spec :rend))
	(unless (eq row (- nrows 1))
	  (insert (maya-plist-get spec :rsep))
	  (when add_lf (insert "\n"))))
      (insert (maya-plist-get spec :end))
      (setq maya--row 1
	    maya--nrows nrows
	    maya--col 1
	    maya--ncols ncols)
      (let ((yas-keymap maya-expand-map))
	(yas/expand-snippet (buffer-substring-no-properties beginp (point))
			    beginp (point))))))

;(bind-key "C-c s m" #'maya-insert-matrix yas-minor-mode-map)

(provide 'matrix-auto-yasnippets)
;;; matrix-auto-yasnippets.el ends here
